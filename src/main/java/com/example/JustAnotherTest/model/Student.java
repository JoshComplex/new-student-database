package com.example.JustAnotherTest.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Document("Students")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    private UUID id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String fullName;
    private String userName;
    private String email;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Student(String firstName, String middleName, String lastName){
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }
}
