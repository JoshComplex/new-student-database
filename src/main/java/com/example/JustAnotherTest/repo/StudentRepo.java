package com.example.JustAnotherTest.repo;

import com.example.JustAnotherTest.model.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface StudentRepo extends MongoRepository<Student, UUID> {

    List<Student> findByFullNameIgnoreCase(String fullName);

}
