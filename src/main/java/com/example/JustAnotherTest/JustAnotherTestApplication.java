package com.example.JustAnotherTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JustAnotherTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JustAnotherTestApplication.class, args);
	}

}
