package com.example.JustAnotherTest.api;

import com.example.JustAnotherTest.model.Student;
import com.example.JustAnotherTest.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/add")
    public Student addStudent(@RequestBody Student student){
        return this.studentService.addStudent(student);
    }

    @GetMapping("/{id}")
    public Optional<Student> findById(@PathVariable(name="id") UUID id) {
        return this.studentService.findById(id);
    }

    @GetMapping
    public List<Student> findAll(){
        return this.studentService.findAll();
    }

    @GetMapping("/fullName/{fullName}")
    public List<Student> findByFullName(@PathVariable(name="fullName") String fullName) {
        return this.studentService.findByFullName(fullName);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable(name="id") UUID id) {
        studentService.deleteById(id);
    }

    @PatchMapping("/{id}")
    public void patchStudent(@RequestBody Student updateStudent, @PathVariable(name="id") UUID id) {
        studentService.patchStudent(updateStudent, id);
    }
}
