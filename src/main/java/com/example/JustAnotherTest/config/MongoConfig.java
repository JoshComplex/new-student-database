package com.example.JustAnotherTest.config;

import com.example.JustAnotherTest.repo.StudentRepo;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = StudentRepo.class)
@Configuration
public class MongoConfig {
}
