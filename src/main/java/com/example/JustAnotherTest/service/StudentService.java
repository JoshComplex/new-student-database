package com.example.JustAnotherTest.service;

import com.example.JustAnotherTest.model.Student;
import com.example.JustAnotherTest.repo.StudentRepo;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class StudentService {

    private StudentRepo studentRepo;

    @Autowired
    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }

    //TODO: Increment username if user found with same name (e.g. Sam Smith duplicate would be ssmith and ssmith1)
    public Student addStudent(Student student){
        //.. random UUID
        student.setId(UUID.randomUUID());

        //.. updating email and username from name criteria
        this.updateStudentDetailsFromNames(student);

        //.. saving student to the repo
        return this.studentRepo.save(student);
    }

    public List<Student> findAll() {
        return this.studentRepo.findAll();
    }

    public Optional<Student> findById(UUID id) {
        return this.studentRepo.findById(id);
    }

    public List<Student> findByFullName(String fullName) {
        return this.studentRepo.findByFullNameIgnoreCase(fullName);
    }

    public void deleteById(UUID id) {
        studentRepo.deleteById(id);
    }

    public void patchStudent(Student updateStudent, UUID id) {

        Optional<Student> student = findById(id);
        if(student.isPresent()){

            if(updateStudent.getFirstName() != null) {
                student.get().setFirstName(updateStudent.getFirstName());
            }

            if(updateStudent.getMiddleName() != null){
                student.get().setMiddleName(updateStudent.getMiddleName());
            }

            if(updateStudent.getLastName() != null){
                student.get().setLastName(updateStudent.getLastName());
            }

            Student updatedStudent = this.updateStudentDetailsFromNames(student.get());

            this.studentRepo.save(updatedStudent);

        }

        else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Student not found");

    }

    public Student updateStudentDetailsFromNames(Student student){

        //.. checking if the student has a middle name optional entry
        boolean hasMiddleName = student.getMiddleName() != null && !student.getMiddleName().isEmpty();

        //.. setting the full name by appending the first and last names with the middle
        //.. in between if the optional field is filled
        student.setFullName(student.getFirstName() + " "
                + (hasMiddleName ? student.getMiddleName() + " " : "")
                + student.getLastName());

        //.. setting the username by getting the first character of the first name (required field),
        //.. the first character of the middle name if it exists (optional field),
        //.. and the full last name
        student.setUserName((student.getFirstName().charAt(0)
                + (hasMiddleName ? String.valueOf(student.getMiddleName().charAt(0)) : "")
                + student.getLastName()).toLowerCase());

        //.. setting the email using the username and the client requested email domain
        student.setEmail(student.getUserName() + "@student.fullsail.edu");

        return student;

    }
}
