package com.example.JustAnotherTest.api;

import com.example.JustAnotherTest.model.Student;
import com.example.JustAnotherTest.repo.StudentRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentController_IntTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private StudentRepo repo;

    private Student student;

    private UUID id;

    @BeforeEach
    void beforeEach(){
        id = UUID.randomUUID();
        this.student = new Student("Joshua", "Brooks", "Shevach");
        this.student.setId(id);
        repo.save(this.student);
    }

    @Test
    void addStudent_IntTest() throws Exception {

        //TODO: stop doing e2e testing
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(student);

        mvc.perform(post("/api/student/add")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    void deleteById_IntTest() throws Exception {

        //TODO: stop doing e2e testing
        mvc.perform(delete("/api/student/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    void findById_IntTest() throws Exception {

        //TODO: stop doing e2e testing
        mvc.perform(get("/api/student/{id}", UUID.randomUUID())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    void findByFullName_IntTest() throws Exception {

        //TODO: stop doing e2e testing
        mvc.perform(get("/api/student/fullName/{fullName}", "Joshua Brooks Shevach")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    void findAll_IntTest() throws Exception {

        //TODO: stop doing e2e testing
        mvc.perform(get("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    void patch_IntTest() throws Exception {

        //TODO: stop doing e2e testing\
        Student student = new Student("Joshua", "", "Shevach");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(student);

        mvc.perform(patch("/api/student/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

    }
}
