package com.example.JustAnotherTest.api;

import com.example.JustAnotherTest.model.Student;
import com.example.JustAnotherTest.service.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StudentController_Tests {

    @InjectMocks
    private StudentController controller;

    @Mock
    private StudentService service;

    private Student student;

    @BeforeEach
    public void beforeTests(){
        this.student = new Student("Joshua", "Shevach");
    }

    @Test
    public void findById_Test() {
        //Arrange
        when(service.findById(any(UUID.class))).thenReturn(Optional.ofNullable(this.student));

        //Act
        Optional<Student> student = controller.findById(UUID.randomUUID());

        //Assert
        assertEquals(Optional.ofNullable(this.student), student);
        verify(service, times(1)).findById(any(UUID.class));
    }

    @Test
    public void findAll_Test(){
        //Arrange
        List<Student> students = Collections.singletonList(this.student);
        when(service.findAll()).thenReturn(students);

        //Act
        List<Student> studentList = controller.findAll();

        //Assert
        assertEquals(students, studentList);
        verify(service, Mockito.times(1)).findAll();
    }

    @Test
    public void addStudent_Test(){
        //Arrange
        when(service.addStudent(this.student)).thenReturn(this.student);

        //Act
        Student student = controller.addStudent(this.student);

        //Assert
        assertEquals(this.student, student);
        verify(service, Mockito.times(1)).addStudent(this.student);
    }

    @Test
    public void findByFullName_Test(){
        //Arrange
        when(service.findByFullName("joshua brooks shevach")).thenReturn(Collections.singletonList(this.student));

        //Act
        List<Student> students = controller.findByFullName("joshua brooks shevach");

        //Assert
        assertEquals(Collections.singletonList(this.student), students);
        verify(service, Mockito.times(1)).findByFullName(any(String.class));
    }

    @Test
    public void deleteById_Test(){

        UUID id = UUID.randomUUID();
        doNothing().when(service).deleteById(id);

        controller.deleteById(id);

        verify(service, Mockito.times(1)).deleteById(id);

    }

    @Test
    public void patch_Test(){

        UUID id = UUID.randomUUID();
        Student updateStudent = new Student("Joshua", "Brooks", "Shevach");
        doNothing().when(service).patchStudent(any(Student.class), any(UUID.class));

        controller.patchStudent(updateStudent, id);

        verify(service, Mockito.times(1)).patchStudent(any(Student.class), any(UUID.class));

    }

}
