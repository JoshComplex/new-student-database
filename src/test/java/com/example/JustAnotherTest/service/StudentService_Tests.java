package com.example.JustAnotherTest.service;

import com.example.JustAnotherTest.model.Student;
import com.example.JustAnotherTest.repo.StudentRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StudentService_Tests {

    @InjectMocks
    private StudentService service;

    @Mock
    private StudentRepo repo;

    private Student student;

    @Test
    public void addStudent_TestForUsername(){
        //Arrange
        this.student = new Student("Raynor", "Blevins", "Corview");
        when(repo.save(this.student)).thenReturn(this.student);

        //Act
        Student student = service.addStudent(this.student);

        //Assert
        assertEquals("rbcorview", student.getUserName());
        Mockito.verify(repo, Mockito.times(1)).save(this.student);
    }

    @Test
    public void addStudent_TestForUsername2(){
        //Arrange
        this.student = new Student("Jensen", "Frederick");
        when(repo.save(this.student)).thenReturn(this.student);

        //Act
        Student student = service.addStudent(this.student);

        //Assert
        assertEquals("jfrederick", student.getUserName());
        Mockito.verify(repo, Mockito.times(1)).save(this.student);
    }

    @Test
    public void addStudent_TestForEmail(){
        //Arrange
        this.student = new Student("Freya", "Lynette", "Goldberg");
        when(repo.save(this.student)).thenReturn(this.student);

        //Act
        Student student = service.addStudent(this.student);

        //Assert
        assertEquals("flgoldberg@student.fullsail.edu", student.getEmail());
        Mockito.verify(repo, Mockito.times(1)).save(this.student);
    }

    @Test
    public void addStudent_TestForEmail2(){
        //Arrange
        this.student = new Student("Joshua", "Shevach");
        when(repo.save(this.student)).thenReturn(this.student);

        //Act
        Student student = service.addStudent(this.student);

        //Assert
        assertEquals("jshevach@student.fullsail.edu", student.getEmail());
        Mockito.verify(repo, Mockito.times(1)).save(this.student);
    }

    @Test
    public void getAll_TestWithOne(){
        //Arrange
        List<Student> studentList = Collections.singletonList(
                new Student("Selene", "Fullbright")
        );

        when(repo.findAll()).thenReturn(studentList);

        //Act
        List<Student> students = service.findAll();

        //Assert
        assertEquals(studentList, students);
        verify(repo, Mockito.times(1)).findAll();
    }

    @Test
    public void getAll_TestWithMany(){
        //Arrange
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("Frank", "Mursmire"));
        studentList.add(new Student("Amon", "Vladiscovias"));
        studentList.add(new Student("Alexandra", "Mugen"));
        studentList.add(new Student("James", "Fin", "Kennedy"));

        when(repo.findAll()).thenReturn(studentList);

        //Act
        List<Student> students = service.findAll();

        //Assert
        assertEquals(studentList, students);
        verify(repo, Mockito.times(1)).findAll();
    }

    @Test
    public void getById_Test(){
        //Arrange
        this.student = new Student("Joshua", "Brooks", "Shevach");
        when(repo.findById(any(UUID.class))).thenReturn(Optional.ofNullable(this.student));

        //Act
        Optional<Student> student = service.findById(UUID.randomUUID());

        //Assert
        assertEquals(Optional.ofNullable(this.student), student);
        verify(repo, Mockito.times(1)).findById(any(UUID.class));
    }

    @Test
    public void getByFullName_TestWithOne(){
        //Arrange
        this.student = new Student("Joshua", "Brooks", "Shevach");
        when(repo.findByFullNameIgnoreCase("joshua brooks shevach")).thenReturn(Collections.singletonList(this.student));

        //Act
        List<Student> students = service.findByFullName("joshua brooks shevach");

        //Assert
        assertEquals(Collections.singletonList(this.student), students);
        verify(repo, Mockito.times(1)).findByFullNameIgnoreCase(any(String.class));
    }

    @Test
    public void getByFullName_TestWithMany(){

        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("Joshua", "Brooks", "Shevach"));
        studentList.add(new Student("Joshua", "Brooks", "Shevach"));
        when(repo.findByFullNameIgnoreCase("joshua brooks shevach")).thenReturn(studentList);

        List<Student> students = service.findByFullName("joshua brooks shevach");

        assertEquals(studentList, students);
        verify(repo, Mockito.times(1)).findByFullNameIgnoreCase(any(String.class));
    }

    @Test
    public void deleteById_Test(){

        UUID id = UUID.randomUUID();
        doNothing().when(repo).deleteById(id);

        service.deleteById(id);

        verify(repo, Mockito.times(1)).deleteById(id);

    }

    @Test
    public void patch_Test(){

        UUID id = UUID.randomUUID();
        this.student = new Student("Joshua", "Shevach");
        Student updateStudent = new Student("Joshua", "Brooks", "Shevach");
        when(repo.findById(any(UUID.class))).thenReturn(Optional.ofNullable(this.student));
        when(repo.save(any(Student.class))).thenReturn(this.student);

        service.patchStudent(updateStudent, id);

        assertEquals(updateStudent.getMiddleName(), student.getMiddleName());
        verify(repo, Mockito.times(1)).findById(any(UUID.class));
        verify(repo, Mockito.times(1)).save(any(Student.class));

    }

}
